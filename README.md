I.PROJECT NAME

Cadenza_cart_management

II.PREREQUISITES

i)Node.js
ii)Node Package Manager(npm)

III.Running Locally

git clone https://gitlab.com/Kuppani/cart_management.git
cd cart_management
npm install
npm start or node server.js or nodemon start server.js

Your app should now be running on localhost:7085