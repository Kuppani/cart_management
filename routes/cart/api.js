const router = require('express').Router();
Cart = require('../../models/cart');
const Methods = require('../../methods/custom');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');
const session		=	require('express-session');

router.use(session({cookieName: 'session',secret: 'ssshhhhh',saveUninitialized: true,resave: true}));

const apiOperation = (req, res, crudMethod, optionsObj)=> {
	const bodyParams = Methods.initializer(req, Cart);
	console.log('bodyParams: ', bodyParams);
	crudMethod(bodyParams, optionsObj, (err, cart) => {
		console.log('\nproduct details', cart);
		res.send(cart);
	})
}

router.get('/token', (req, res) => {
	const sess=req.session;
	console.log("welocome token "+sess.user);
});

router.post('/getcart', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+'_cart_data';
					Cart.selectTable(table);
					apiOperation(req, res, Cart.getItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/addcart', (req, res) => {
	// Here body-params should contain the name of the table, cartId etc.
	// For now we are using the tablename, soon we will switch this to shopid
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
				const bodyParams = Methods.initializer(req, Cart);
				Cart.createItem(bodyParams, {
					table: decode.storeName+"_cart_data",
					overwrite: false
				}, (err, cart) => {
					console.log('\ncart data\n', cart);
					res.send(cart);
				})
			}
        })
    }
    else{
        res.send("please send a token");
    }
});

router.put('/updatecart', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+'_cart_data';
					Cart.selectTable(table);
					apiOperation(req, res, Cart.updateItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.delete('/deletecart', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+'_cart_data';
					Cart.selectTable(table);

                    const removeCallback = (err, removeData) => {
                        console.log('\nThe removed item data is...\n', removeData);
                        res.send(removeData);
                    }
              
                    // passing the conditional object here {} as second parameter
                    Cart.deleteItem({
						cart_id: req.body.cart_id,
						user_id: req.body.user_id
                    }, 
                    {ReturnValues: 'ALL_OLD'}
                    ,removeCallback);
					//apiOperation(req, res, Cart.deleteItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

module.exports = router;